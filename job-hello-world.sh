#!/bin/bash

#SBATCH --job-name="hello-world"
#SBATCH -o %A.out
#SBATCH -e %A.err
#SBATCH -p test.q
#SBATCH -t 00:05:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2

source init-mpi

mpirun -np 2 ./hello-world
