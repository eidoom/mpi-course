#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    /* initialise data */
    enum tag {
        tag1,
        tag2,
    };
    enum proc {
        proc1,
        proc2,
    };
    const int count = 5, loops = 2;
    int sbuf[count], rbuf[count], rcount, i, size, rank;
    MPI_Status status;

    /* initialise MPI */
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == proc1) {
        sbuf[0] = 0;
    }

    for (i = 0; i < loops; ++i) {

        switch (rank) {
        case proc1:

            /* blocking send on first processor to second AND receive on first processor from second */
            /* MPI_Sendrecv(&sbuf[0], count, MPI_INT, proc2, tag1, &rbuf[0], count, MPI_INT, proc2, tag2, MPI_COMM_WORLD, &status); */

            /* blocking send on first processor to second */
            MPI_Ssend(&sbuf[0], count, MPI_INT, proc2, tag1, MPI_COMM_WORLD);

            /* blocking receive on first processor from second */
            MPI_Recv(&rbuf[0], count, MPI_INT, proc2, tag2, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &rcount);

            /* alter data for next loop */
            sbuf[0] = ++rbuf[0];
            break;

        case proc2:
            /* blocking receive on second processor from first */
            MPI_Recv(&rbuf[0], count, MPI_INT, proc1, tag1, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &rcount);

            /* alter data to send back again */
            sbuf[0] = ++rbuf[0];

            /* blocking send on second processor to first */
            MPI_Ssend(&sbuf[0], count, MPI_INT, proc1, tag2, MPI_COMM_WORLD);
            break;
        }

        printf("proc %d: value=%d\n", rank, sbuf[0]);
        printf("        rstatus: src=%d scnt=%d rcnt=%d tag=%d err=%d\n", status.MPI_SOURCE, count, rcount, status.MPI_TAG, status.MPI_ERROR);
    }

    MPI_Finalize();
}
