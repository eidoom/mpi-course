#!/usr/bin/env bash

source init-mpi
make -j
mpirun -np 2 ./hello-world
