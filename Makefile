.PHONY: all

all: hello-world ping-pong

hello-world: hello-world.c
	mpicc -o $@ $<

ping-pong: ping-pong.c
	mpicc -o $@ $<
